/*
*
* 2017 © Juho Haapalainen
*
 */

package timo;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


// A class that reads XML-file and contains all the information of SmartPosts.
public class SmartPostData {
    
    private Document d;
    private ArrayList<SmartPost> smartPosts;
    
    // Only one instance of SmartPostData can be created (Singleton-principle).
    static private SmartPostData spd = null;
    
    private SmartPostData() {
        try {
            smartPosts = loadSmartPosts();
        } catch (IOException | SAXException | ParserConfigurationException ex) {
            Logger.getLogger(SmartPostData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static public SmartPostData getInstance() {
        if (spd == null) {
            spd = new SmartPostData();
        }
        return spd;
    }
    
    public ArrayList<SmartPost> getSmartPosts() {
        return smartPosts;
    }
    
    // Returns a list of all the cities in SmartPostData
    public ArrayList<String> getCities() {
        ArrayList<String> cities = new ArrayList();
        for (SmartPost sp : smartPosts) {
            if (!cities.contains(sp.getCity())) {
                cities.add(sp.getCity());
            }
        }
        return cities;
    }
    
    // A function that does all the XML-reading and parsing. Returns a list of all the SmartPosts in Finland.
    private ArrayList<SmartPost> loadSmartPosts() throws MalformedURLException, IOException, SAXException, ParserConfigurationException {
        
        ArrayList<SmartPost> list = new ArrayList();
        URL url = new URL("http://smartpost.ee/fi_apt.xml");
        InputStream s = url.openStream();
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        d = dBuilder.parse(s);
        d.getDocumentElement().normalize();
        d.getDocumentElement();
        NodeList nodes = d.getElementsByTagName("place");
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            String code = e.getElementsByTagName("code").item(0).getTextContent();
            String city = e.getElementsByTagName("city").item(0).getTextContent();
            String address = e.getElementsByTagName("address").item(0).getTextContent();
            String availability = e.getElementsByTagName("availability").item(0).getTextContent();
            String postoffice = e.getElementsByTagName("postoffice").item(0).getTextContent();
            double lat = Double.parseDouble(e.getElementsByTagName("lat").item(0).getTextContent());
            double lng = Double.parseDouble(e.getElementsByTagName("lng").item(0).getTextContent());

            list.add(new SmartPost(code, city, address, availability, postoffice, lat, lng ));
        }
        return list;
    }

    
}


/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;


public class FXMLCreateParcelViewController implements Initializable {

    @FXML
    private ComboBox<Item> chooseItemCombo;
    @FXML
    private ComboBox<String> chooseDepartureCityCombo;
    @FXML
    private ComboBox<SmartPost> chooseDepartureSmartPostCombo;
    @FXML
    private ComboBox<String> chooseArrivalCityCombo;
    @FXML
    private ComboBox<SmartPost> chooseArrivalSmartPostCombo;
    @FXML
    private Button createAndStoreParcelButton;
    @FXML
    private TextField screen;

    ToggleGroup group = new ToggleGroup();
    @FXML
    private RadioButton radio1;
    @FXML
    private RadioButton radio2;
    @FXML
    private RadioButton radio3;
    
    SmartPostData spd = SmartPostData.getInstance();
    Storage storage = Storage.getInstance();
    StorageData storageData = StorageData.getInstance();
    
    ArrayList<SmartPost> smartPosts = spd.getSmartPosts();
    ArrayList<String> cities = spd.getCities();
    Parcel currentParcel;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {       
        
        chooseItemCombo.getItems().add(new Carillo());
        chooseItemCombo.getItems().add(new Karjala100Pack());
        chooseItemCombo.getItems().add(new RussianStandard());
        chooseItemCombo.getItems().add(new Liisa());
        chooseItemCombo.getItems().add(new OriginalLonkero6Pack());   
        
        radio1.setToggleGroup(group);
        radio2.setToggleGroup(group);
        radio3.setToggleGroup(group);
        radio1.setSelected(true);
        
        fillDepAndArrCityCombo();
    }
    
    // Function creates a parcel, puts an item into it, sets the destination for it 
    // and stores it into the storage. 
    @FXML
    private void createAndStoreParcel(ActionEvent event) {
        
        // Depending on users choice, a parcel of the certain class is created.
        if (radio1.isSelected()) {
            // The given parameter is String ID in form "xxxx".
            currentParcel = new FirstClassParcel(String.format("%04d", storage.counter));           
        } else if (radio2.isSelected()) {
            currentParcel = new SecondClassParcel(String.format("%04d", storage.counter));
        } else if (radio3.isSelected()) {
            currentParcel = new ThirdClassParcel(String.format("%04d", storage.counter));
        }       
        
        if (chooseItemCombo.getValue() != null) {
        
            Item item = chooseItemCombo.getValue();
            
            // The procedure is executed only if user makes all four choices: departure and arrival SmartPost
            // and departure and arrival city
            if (chooseDepartureSmartPostCombo.getValue() != null && chooseArrivalSmartPostCombo.getValue() != null && chooseDepartureCityCombo.getValue() != null && chooseArrivalCityCombo.getValue() != null) {

                // If the item is too large or heavy for that parcel the procedure won't be executed.
                if (item.getWeight() <= currentParcel.getMaxWeight() && item.getSize() <= currentParcel.getMaxSize()) {               

                    currentParcel.setItem(item);
                    if (item.isBreakable) {
                        currentParcel.itemIsBreakable = "Kyllä";
                    } else {
                        currentParcel.itemIsBreakable = "Ei";
                    }
                    // Adding the parcel to the storage
                    storageData.parcelsInStorage.add(currentParcel);
                    storage.addParcel(currentParcel);
                    storage.counter++;
                    //Setting the deparure place and the destination for the parcel
                    currentParcel.setDepartureCity(chooseDepartureCityCombo.getValue());
                    currentParcel.setArrivalCity(chooseArrivalCityCombo.getValue());
                    currentParcel.setDepartureSP(chooseDepartureSmartPostCombo.getValue());
                    currentParcel.setArrivalSP(chooseArrivalSmartPostCombo.getValue());
      
                    chooseDepartureSmartPostCombo.getItems().clear();
                    chooseArrivalSmartPostCombo.getItems().clear();
                    radio1.setSelected(true);
                    screen.setText("Luotiin paketti " + currentParcel.getID() +".");

                } else {
                    screen.setText("Esine ei mahdu pakettiin!");
                    currentParcel = null;
                }  

            } else {
                screen.setText("Ainakin yksi valinnoista puuttuu!");
                currentParcel = null;
            }
        
        } else {
            screen.setText("Ainakin yksi valinnoista puuttuu!");
            currentParcel = null;
        }
    }
    
    // Function fills two combo boxes: "chooseDepartureCityCombo" and "chooseArrivalCityCombo".
    private void fillDepAndArrCityCombo() {
        for (String city : cities) {
            chooseArrivalCityCombo.getItems().add(city);
            chooseDepartureCityCombo.getItems().add(city); 
        }
    }
    
    // Function fills combobox "chooseDepartureSmartPostCombo" chosing only SmartPosts that locate
    // in that particular city
    @FXML
    private void fillDepSPCombo(ActionEvent event) {
        chooseDepartureSmartPostCombo.getItems().clear();
        for (SmartPost sp : smartPosts) {
            if (chooseDepartureCityCombo.getValue().equals(sp.getCity())) {
                chooseDepartureSmartPostCombo.getItems().add(sp);
            }
        }  
    }

    // Function fills combobox "chooseArrivalSmartPostCombo" chosing only SmartPosts that locate
    // in that particular city
    @FXML
    private void fillArrSPCombo(ActionEvent event) {
        chooseArrivalSmartPostCombo.getItems().clear();
        for (SmartPost sp : smartPosts) {
            if (chooseArrivalCityCombo.getValue().equals(sp.getCity())) {
                chooseArrivalSmartPostCombo.getItems().add(sp);
            }
        }
    }
           
}

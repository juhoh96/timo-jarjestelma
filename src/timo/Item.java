/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;


public abstract class Item implements java.io.Serializable {
   
    protected String name;
    protected double size; // Size in dm3
    protected double weight; // Weight in kg
    public boolean isBroken; // Tells if the item is broken
    public boolean isBreakable; // Tells if the item can be broke by the TIMO-man

    public String getName() {
        return name;
    }

    public double getSize() {
        return size;
    }

    public double getWeight() {
        return weight;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
}

// In Carillo, one can taste mansikkahillo
class Carillo extends Item implements java.io.Serializable {
    
    public Carillo() {
        this.name = "Carillo";
        this.size = 0.75;
        this.weight = 0.75;
        this.isBroken = false;
        this.isBreakable = true; // Breaks easily
    }    
}

// What would be better way to celebrate the 100-hundred-year independence than drinking 100 hundred 
// cans of keskiolut?
class Karjala100Pack extends Item implements java.io.Serializable {
    
    public Karjala100Pack() { 
        this.name = "Karjala 100-pack"; 
        this.size = 50; // Very big and heavy item, only 2. class parcels can contain it.
        this.weight = 33.3;
        this.isBroken = false;
        this.isBreakable = false;
    }
}

// Brilliant kyykky-viini in Alko, I highly recommend tasting it.
class Liisa extends Item implements java.io.Serializable {
    
    public Liisa() {
        this.name = "Liisa Sauvignon Blanc";   
        this.size = 1;
        this.weight = 1.0;
        this.isBroken = false;
        this.isBreakable = false;
    }
}

// Good tasting and affordable vodka for authentic Russian experience.
class RussianStandard extends Item implements java.io.Serializable {
    
    public RussianStandard() { 
        this.name = "Russian Standard Vodka"; 
        this.size = 1;
        this.weight = 1.0;
        this.isBroken = false;
        this.isBreakable = true;
    }
}

// A basic six-pack of lonkero.
class OriginalLonkero6Pack extends Item implements java.io.Serializable {
    
    public OriginalLonkero6Pack() {
        this.name = "OriGINal Long Drink 6-pack"; 
        this.size = 10;
        this.weight = 1.0;
        this.isBroken = false;
        this.isBreakable = false;
    }
}
/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;

// A class that handles all the serialization
public class SerOperator {
    
    // Serializes the data and saves it in a file "storagedata.ser" so that it can be loaded from there later.
    public void saveStorageData(StorageData s) {
        
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("storagedata.ser"));
            // ObservableList cannot be serialized so it must be converted into ArrayList form.
            out.writeObject(new ArrayList<>(s.parcelsInStorage));
            out.writeObject(new ArrayList<>(s.sentParcels));
            out.close();
        } catch (IOException i ) {
            i.printStackTrace();
        }
    }
    
    // Function loads the serialized data from a file and returns a StorageData instance.
    public StorageData loadStorageData() {
        
        StorageData s = StorageData.getInstance();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("storagedata.ser"));
            // As the ObservableList was converted into ArrayList, now the ArrayList must be converted
            // back to ObservableList.
            ArrayList<Parcel> list1 = (ArrayList<Parcel>) in.readObject();
            s.parcelsInStorage = FXCollections.observableList(list1);
            ArrayList<Parcel> list2 = (ArrayList<Parcel>) in.readObject();
            s.sentParcels = FXCollections.observableList(list2);
            in.close();
        } catch (IOException i ) {
            i.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerOperator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
    }
    
}

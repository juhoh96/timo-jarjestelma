/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;

import java.util.ArrayList;

// A class that stores parcels. 
public class Storage {   
    
    public ArrayList<Parcel> parcels;
    public int counter; // Counts how many parcels there have been in total in the storage.
    
    static private Storage storage = null;
    
    // Storage-class follows Singleton-principle, because all the parcels must be in the very same storage.
    private Storage() {
        parcels = new ArrayList();
        counter = 1;
    }
    
    static public Storage getInstance() {
        if (storage == null) {
            storage = new Storage();
        }
        return storage;
    }
    
    // Adds the given parcel to the storage.
    public void addParcel(Parcel p) {
        this.parcels.add(p);
    }
    
    // Removes the parcel from the storage.
    public void removeParcel(Parcel p) {
        this.parcels.remove(p);
    } 
    
}

/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;


public class FXMLStorageViewController implements Initializable {

    StorageData storageData = StorageData.getInstance();
    
    @FXML
    private TableView<Parcel> parcelsInStorageTable;
    @FXML
    private TableColumn<Parcel, String> idColumn1;
    @FXML
    private TableColumn<Parcel, String> classColumn1;
    @FXML
    private TableColumn<Parcel, String> itemColumn1;
    @FXML
    private TableColumn<Parcel, String> depCityColumn1;
    @FXML
    private TableColumn<Parcel, String> depSPColumn1;
    @FXML
    private TableColumn<Parcel, String> arrCityColumn1;
    @FXML
    private TableColumn<Parcel, String> arrSPColumn1;
    @FXML
    private TableColumn<Parcel, String> distanceColumn1;
    @FXML
    private TableColumn<Parcel, String> isItBreakableColumn;
    
    @FXML
    private TableView<Parcel> sentParcelsTable;
    @FXML
    private TableColumn<Parcel, String> idColumn2;
    @FXML
    private TableColumn<Parcel, String> classColumn2;
    @FXML
    private TableColumn<Parcel, String> itemColumn2;
    @FXML
    private TableColumn<Parcel, String> depCityColumn2;
    @FXML
    private TableColumn<Parcel, String> depSPColumn2;
    @FXML
    private TableColumn<Parcel, String> arrCityColumn2;
    @FXML
    private TableColumn<Parcel, String> arrSPColumn2;    
    @FXML
    private TableColumn<Parcel, String> distanceColumn2;
    @FXML
    private TableColumn<Parcel, String> didItBreakColumn;
    
    @FXML
    private Label timoLabel;
    @FXML
    private AnchorPane anchor;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
      
    // Fills two TableView components: "parcelsInStorageTable" and "sentParcelsTable"
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        parcelsInStorageTable.setItems(storageData.parcelsInStorage);
        
        idColumn1.setCellValueFactory(new PropertyValueFactory<>("ID"));
        classColumn1.setCellValueFactory(new PropertyValueFactory<>("parcelClass"));
        itemColumn1.setCellValueFactory(new PropertyValueFactory<>("item"));
        depCityColumn1.setCellValueFactory(new PropertyValueFactory<>("departureCity"));
        depSPColumn1.setCellValueFactory(new PropertyValueFactory<>("departureSP"));
        arrCityColumn1.setCellValueFactory(new PropertyValueFactory<>("arrivalCity"));
        arrSPColumn1.setCellValueFactory(new PropertyValueFactory<>("arrivalSP"));
        distanceColumn1.setCellValueFactory(new PropertyValueFactory<>("distance"));
        isItBreakableColumn.setCellValueFactory(new PropertyValueFactory<>("itemIsBreakable"));
                
        sentParcelsTable.setItems(storageData.sentParcels);
        
        idColumn2.setCellValueFactory(new PropertyValueFactory<>("ID"));
        classColumn2.setCellValueFactory(new PropertyValueFactory<>("parcelClass"));
        itemColumn2.setCellValueFactory(new PropertyValueFactory<>("item"));
        depCityColumn2.setCellValueFactory(new PropertyValueFactory<>("departureCity"));
        depSPColumn2.setCellValueFactory(new PropertyValueFactory<>("departureSP"));
        arrCityColumn2.setCellValueFactory(new PropertyValueFactory<>("arrivalCity"));
        arrSPColumn2.setCellValueFactory(new PropertyValueFactory<>("arrivalSP"));
        distanceColumn2.setCellValueFactory(new PropertyValueFactory<>("distance"));
        didItBreakColumn.setCellValueFactory(new PropertyValueFactory<>("itemBroken"));

        // Storage view doesn't use the same CSS file so the styling is done here.
        Image timoLogo = new Image(getClass().getResourceAsStream("timoLogo.png"));
        timoLabel.setGraphic(new ImageView(timoLogo));
        anchor.setStyle("-fx-background-color: white;");
    }       

}
